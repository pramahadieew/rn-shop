import Product from '../models/product';

const PRODUCT = [
    new Product(
        'p1',
        'u1',
        'Buku Tulis',
        'https://cdn.pixabay.com/photo/2015/10/03/02/14/pen-969298_1280.jpg',
        'Yes Men',
        47000
    ),
    new Product(
        'p2',
        'u1',
        'Karpet Masjid',
        'https://images.pexels.com/photos/6292/blue-pattern-texture-macro.jpg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
        'Fits your red shirt perfectly. To stand on. Not to wear it.',
        95000
    ),
    // new Product(
    // 'p3',
    // 'u2',
    // 'Gelas Kopi',
    // 'https://images.pexels.com/photos/160834/coffee-cup-and-saucer-black-coffee-loose-coffee-beans-160834.jpeg?cs=srgb&dl=bean-beans-black-coffee-160834.jpg&fm=jpg',
    // 'Can also be used for tea!',
    // 25000
    // ),
];

export default PRODUCT;