import React from 'react';

import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider }   from "react-redux";
import ReduxThunk from 'redux-thunk';

import cartReducer    from './store/reducers/cart';
import productReducer from './store/reducers/products';
import orderReducer from './store/reducers/orders';
import ShopNavigator  from './navigation/ShopNavigator';

// import { AppLoading } from 'expo';
// import * as Font from 'expo-font';

const rootReducer = combineReducers({
  products: productReducer,
  cart: cartReducer,
  orders: orderReducer
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

// const fetchFont = () => {
//   return Font.loadAsync({
//     'open-sans': require('./assets/fonts/OpenSans-Reguler.ttf'),
//     'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
//   })
// }

export default function App() {
  // const [fontLoaded, setFontLoaded] = useState(false);

  // if (!fontLoaded) {
  //   <AppLoading 
  //     startAsync={fetchFont} 
  //     onFinish={() => {
  //       setFontLoaded(true);
  //   }}/>
  // }
  return (
    <Provider store={store}>
        <ShopNavigator />
    </Provider>
  );
};
